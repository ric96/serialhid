/*
  Keyboard test

  For the Arduino Leonardo, Micro or Due

  Reads a byte from the serial port, sends a keystroke back.
  The sent keystroke is one higher than what's received, e.g. if you send a,
  you get b, send A you get B, and so forth.

  The circuit:
  - none

  created 21 Oct 2011
  modified 27 Mar 2012
  by Tom Igoe

  This example code is in the public domain.

  http://www.arduino.cc/en/Tutorial/KeyboardSerial
*/

#include "Keyboard.h"

char* inChar;
int press_flag;
int key;

void setup() {
  // open the serial port:
  Serial1.begin(115200);
  Serial1.setTimeout(10);
  // initialize control over the keyboard:
  Keyboard.begin();
}

void loop() {
  // check for incoming serial data:
  if (Serial1.available() > 0) {
    // read incoming serial data:
    Serial.readBytes(inChar, 6);
    sscanf(inChar, "%d %d", key, press_flag);
      if(press_flag == 0)
        Keyboard.press(key);
      else if(press_flag == 1)
        Keyboard.release(key);
  }
}
