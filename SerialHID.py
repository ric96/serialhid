import keyboard
import mouse
import sys
import serial
import signal, os


ser = serial.Serial('/dev/ttyUSB0', 115200, timeout=0.010)

global rec_kbd

status = 0
#ser.open()

def send(val):
    buf = "%s %s" % (val, status)
    #ser.open()
    ser.write(buf.encode())
    ser.flush()
    #print(buf)
    #ser.close()

s = signal.signal(signal.SIGINT, signal.SIG_IGN)


while(1):
    rec_kbd = keyboard.read_event()
    if rec_kbd.event_type == 'up':
        status = 1
    elif rec_kbd.event_type == 'down':
        status = 0
    #print(rec_kbd.name)
    if rec_kbd.name == 'ctrl': send(128)
    elif rec_kbd.name == 'shift': send(129)
    elif rec_kbd.name == 'alt': send(130)
    elif rec_kbd.name =='menu': send(131)             
    elif rec_kbd.name =='up': send(218)
    elif rec_kbd.name =='down': send(217)
    elif rec_kbd.name =='left': send(216)
    elif rec_kbd.name =='right': send(215)
    elif rec_kbd.name =='backspace': send(178)
    elif rec_kbd.name =='tab': send(179)
    elif rec_kbd.name =='enter': send(176)
    elif rec_kbd.name =='esc': send(177)
    elif rec_kbd.name =='insert': send(209)
    elif rec_kbd.name =='delete': send(212)
    elif rec_kbd.name =='page up': send(211)
    elif rec_kbd.name =='page down': send(214)
    elif rec_kbd.name =='home': send(210)
    elif rec_kbd.name =='end': send(213)
    elif rec_kbd.name =='caps lock': send(193)
    elif rec_kbd.name =='f1': send(194)
    elif rec_kbd.name =='f2': send(195)
    elif rec_kbd.name =='f3': send(196)
    elif rec_kbd.name =='f4': send(197)
    elif rec_kbd.name =='f5': send(198)
    elif rec_kbd.name =='f6': send(199)
    elif rec_kbd.name =='f7': send(200)
    elif rec_kbd.name =='f8': send(201)
    elif rec_kbd.name =='f9': send(202)
    elif rec_kbd.name =='f10': send(203)
    elif rec_kbd.name =='f11': send(204)
    elif rec_kbd.name =='f12': send(205)
    elif rec_kbd.name =='f13': send(240)
    elif rec_kbd.name =='f14': send(241)
    elif rec_kbd.name =='f15': send(242)
    elif rec_kbd.name =='f16': send(243)
    elif rec_kbd.name =='f17': send(244)
    elif rec_kbd.name =='f18': send(245)
    elif rec_kbd.name =='f19': send(246)
    elif rec_kbd.name =='f20': send(247)
    elif rec_kbd.name =='f21': send(248)
    elif rec_kbd.name =='f22': send(249)
    elif rec_kbd.name =='f23': send(250)
    elif rec_kbd.name =='f24': send(251)
    elif rec_kbd.name == 'space': send(32)
    elif(rec_kbd.name == 'q' and rec_kbd.modifiers == ('shift',)):
        sys.exit(0)
    else:
        send(ord(rec_kbd.name))

signal.signal(signal.SIGINT, s)